import React from 'react';
import { User } from '../../models/user';

interface UserCardProps {
  user: User;
  onClick?: () => void;
  selected?: boolean;
}

export const UserCard: React.FC<UserCardProps> = ({
  user,
  onClick,
  selected
}: UserCardProps) => {
  return (
    <div className={`card ${selected ? 'selected' : ''}`} onClick={onClick}>
      <h2>{user.name}</h2>
    </div>
  );
};
