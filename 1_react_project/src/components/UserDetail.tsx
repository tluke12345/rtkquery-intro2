import React from 'react';
import { User } from '../../models/user';

interface UserDetailProps {
  user: User;
}

export const UserDetail: React.FC<UserDetailProps> = ({
  user
}: UserDetailProps) => {
  return (
    <div className="user-detail">
      <h2>{user.name} Details</h2>
      <p>Hobby: {user.hobby}</p>
    </div>
  );
};
