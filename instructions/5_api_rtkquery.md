# Connect with the API using RTK query

Now that we understand the problem, lets investigate how rtk and rtk query
can help.

## step

### Initial project

- Copy 3_react_ui to 5_api_rtkquery

### Add rtk

- Add rtk to the project
- Implement the api
- Create redux store and add the api to it
- Add the store to the app
- test the new api

### User API

- use rtk query to fetch data, loading and error state

### User Details

- use rtk query to fetch user details, loading and error state
- Test the new endpoint
- Call the new endpoint when a user is selected

### Reloading data

- Add a button to reload the data
