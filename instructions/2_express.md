# Simple express server

We will now create a simple express server that we can access using RTK query.

## Steps

### Create Project

- First, make a new directory and initialize npm
- Initialize git

### Add Typescript and Express

- Add Typescript to the project
- Add express to the project

### Dev tools

- Install TsNode to run the server
- Install NodeMon to automatically restart the server when we make changes
- Add a 'dev' script to the package.json file
- Add base 'src' directory with 'index.ts' file

### Create the server

- Create the basic server ('src/index.ts')
- Try running the server
- Create some dummy data (see below):

### Add routes

- Add a /users route to the server
- try accessing the route
- Add a /users/detail route to the server (see data below)
- try getting individual users info

## dummy data

```typescript
// In src/index.ts is fine
const users = [
  { id: 1, name: "John" },
  { id: 2, name: "Jane" },
  { id: 3, name: "Jim" },
];

const userDetails = {
  1: {
    id: 1,
    name: "John",
    hobby: "Soccer",
    age: 25,
    country: "USA",
    languages: ["English", "Spanish"],
    friends: [2],
  },
  2: {
    id: 2,
    name: "Jane",
    hobby: "Reading",
    age: 30,
    country: "UK",
    languages: ["English", "French"],
    friends: [1, 3],
  },
  3: {
    id: 3,
    name: "Jim",
    hobby: "Swimming",
    age: 22,
    country: "Australia",
    languages: ["English"],
    friends: [],
  },
};
```
