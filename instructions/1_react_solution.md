# React project

before we can do anythion we need a project to work on. We will create a new
project without using create-react-app. This will give us a better
understanding of the base technologies.

## Create a new project

Create a new directory for the project and navigate into it.

```bash
mkdir react_project
cd react_project
```

Initialize git

```bash
git init -b main
touch .gitignore
```

Initialize npm in the project directory.

```bash
npm init -y
```

Install TypeScript and React.

```bash
npm install react react-dom
npm install --save-dev typescript @types/react @types/react-dom
```

Initialize TypeScript in the project.

```bash
npx tsc --init
```

make sure we can use jsx

```json
{
  "compilerOptions": {
    "jsx": "react"
  }
}
```

Add EsLint and Prettier to the project.

```bash
npm install --save-dev eslint prettier
```

Initialize EsLint in the project.

```bash
npx eslint --init
```

Make sure to select the 'react' and 'typescript' option when configuring EsLint.

Setup Prettier in the project.

```bash
touch .prettierrc
```

Add the following content to the `.prettierrc` file:

```json
{
  "singleQuote": true,
  "trailingComma": false
}
```

Add vite for easy development.

```bash
npm install --save-dev vite
```

Add start, lint, and format scripts to the `package.json` file.

```json
{
  "scripts": {
    "start": "vite",
    "lint": "eslint . --ext .ts,.tsx",
    "format": "prettier --write ."
  }
}
```

Change the `main` field in the `package.json` file to point to the `index.html` file.

```json
{
  "main": "index.html"
}
```

Now we have a basic project setup. We can start adding code to the project.

First create a `src` directory and add a `main.tsx` file.

```bash
mkdir src
touch src/main.tsx
```

Now add the application entry point, the index.html file.

```bash
touch index.html
```

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>React Project</title>
  </head>
  <body>
    <div id="app"></div>
    <script type="module" src="/src/main.tsx"></script>
  </body>
</html>
```

Lets add a simple App component

```bash
touch src/App.tsx
```

```tsx
import React from "react";
export const App = () => {
  return <h1>Hello World</h1>;
};
```

Now we can initialize our website in the `main.tsx` file.

```tsx
import React from "react";
import { createRoot } from "react-dom/client";
import { App } from "./App";

const appElement = document.getElementById("app") as HTMLElement;
const root = createRoot(appElement);
root.render(<App />);
```

Start the APP

```bash
npm start
```

Make sure to commit your changes. (make sure you don't commit the
`node_modules` directory)

```bash
echo "node_modules" > .gitignore
```

```bash
git add .
git commit -m "Initial commit"
```
