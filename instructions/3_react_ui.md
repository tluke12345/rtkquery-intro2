# Simple react UI

(working in the `react_project` directory)

## Steps

### User

- Create a model to represent a user
- Create a User 'Card' component that displays the user information
- Create a css file and style resets, variables, etc.
- Add Some card styles
- Add some hard coded data to the App component and display the UserCards

### User Detail

- Create a model to represent a user detail
- Create a user detail component
- Add some styles for the user detail component
- Add some hard coded data to the App component and display the UserDetail
- Add a user detail section after the list of user cards. This should display
  the currently selected user.
- Set the selected user by clicking on the user card
- Modify the code so that the user card can be clicked again to deselect the user
- Add border around the selected user card
