# Connect with API using fetch

Before we use the tools provided by RTK Query, it is good to understand the
problem we are solving. We well connect the react app with the backend server
using the browser native 'fetch' api.

## Steps

- make a copy of the react app (we will use 3_react_ui as the base for 5 too)

```bash
cp -r 3_react_ui 4_api_fetch
cd 4_api_fetch
```

### Repository

- Create a respository

```bash
mkdir src/repositories
touch src/repositories/userRepository.ts
```

```typescript
// src/repositories/apiRepository.ts
const userURL = "http://localhost:3000/users";

export const fetchUsers = async (): Promise<User[]> => {
  const response = await fetch(userURL);
  if (!response.ok) {
    throw new Error("Failed to fetch users");
  }
  return (await response.json()) as User[];
};
```

### Fetch Users

- Test that the the we can connect

```typescript
// src/App.tsx
import { fetchUsers } from "./repositories/userRepository";
console.log("fetching users...");
const fetchedUsers = fetchUsers();
console.log("fetched users:", fetchedUsers);
// !!! We don't get data... this is a promise...
```

- useEffect to handle async fetch

```typescript
// src/App.tsx
import React, { useEffect, useState } from 'react';
import { fetchUsers } from './repositories/userRepository';

function App() {
  const [users, setUsers] = useState<User[]>([]);
  console.log('fetched users:', users);

  useEffect(() => {
    console.log('fetching users...');
    fetchUsers().then((data) => setUsers(data));
  }, []);

  return (
    // ...
  );
}
```

- show loading ui

```typescript
// src/App.tsx
const [loading, setLoading] = useState(true);
useEffect(() => {
  fetchUsers().then((data) => {
    setUsers(data);
    setLoading(false);
  });
}, []);

return (
  <div>
    { loading && <div>Loading...</div>}
    // ...
  </div>
);
```

- catch errors and show error message

````typescript
// src/App.tsx
const [error, setError] = useState<string | null>(null);
useEffect(() => {
  console.log('fetching users...');
  fetchUsers()
    .then((data) => {
      setUsers(data);
      setLoading(false);
    })
    .catch((error) => {
      setError(error.message);
      setLoading(false);
    });
}, []);

return (
  <div>
    { error && <div>Error: {error}</div> }
    { loading && <div>Loading...</div>}
    //..
  </div>
);
```

- custom hooks to organize code (separate view code and logic code
  -> 'controller' or 'view model')

```typescript
// src/hooks/useUsers.ts
import { useEffect, useState } from 'react';
import { fetchUsers } from '../repositories/userRepository';

export const useUsers = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    fetchUsers()
      .then((data) => {
        setUsers(data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error.message);
        setLoading(false);
      });
  }, []);

  return { users, loading, error };
};
```

```typescript
// src/App.tsx
import { useUsers } from './hooks/useUsers';

function App() {
  const { users, loading, error } = useUsers();
  // ...
  );
}
```
}

### Fetch User Detail

- Create a repository for fetching user detail

```typescript
// src/repositories/userDetailRepository.ts
const userDetailURL = 'http://localhost:3000/users/detail';

export const fetchUserDetail = async (id: number): Promise<UserDetail> => {
  const response = await fetch(`${userDetailURL}?id=${id}`);
  if (!response.ok) {
    throw new Error('Failed to fetch user');
  }
  return (await response.json()) as UserDetail;
};
```

- Create a hook for fetching user detail

```typescript
export const useUserDetail = (id: number) => {
  const [userDetail, setUserDetail] = useState<UserDetail | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    fetchUserDetail(id)
      .then((data) => {
        setUserDetail(data);
        setLoading(false);
      })
      .catch((e) => {
        setError(e);
        setLoading(false);
      });
  }, [id]);

  return { userDetail, loading, error };
};
```

- Modify userDetail component with using hook, and show loading and error

```typescript
// src/components/UserDetail.tsx
interface UserDetailProps {
  id: number;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  id,
}: UserDetailProps) => {
  const { userDetail, loading, error } = useUserDetail(id);
  return (
    <div className="user-detail">
      {loading && <p>Loading...</p>}
      {error && <p>Error: {error.message}</p>}
      {userDetail && (
        <>
          <h2>{userDetail.name}</h2>
          <p>Hobby: {userDetail.hobby}</p>
          <p>Age: {userDetail.age}</p>
          <p>Country: {userDetail.country}</p>
          <p>Languages: {userDetail.languages.join(', ')}</p>
          <p>Friends: {userDetail.friends.join(', ')}</p>
        </>
      )}
    </div>
  );
};
```

- Test the app. Watch the network tab. When you click on a user the detail API
  is called. Note that the same api is called multiple times for the same user.

### Caching


- We need to elevate the state to the App component to cache the user detail

```typescript
// src/App.tsx

  const [userDetailCache, setUserDetailCache] = useState<{ [key: number]: UserDetail }>({});

  const [isLoadingUserDetail, setIsLoadingUserDetail] = useState<boolean>(false);
  const [userDetailError, setUserDetailError] = useState<Error | null>(null);
  const [selectedUserDetail, setSelectedUserDetail] = useState<UserDetail | null>(null);

  useEffect(() => {
    if (!selectedUser) {
      return;
    }

    if (userDetailCache[selectedUser.id]) {
      setSelectedUserDetail(userDetailCache[selectedUser.id]);
      return;
    }

    setIsLoadingUserDetail(true);
    setUserDetailError(null);
    fetchUserDetail(selectedUser.id).then((data) => {
      setUserDetailCache((prevCache) => ({ ...prevCache, [selectedUser.id]: data }));
      setSelectedUserDetail(data);
      setIsLoadingUserDetail(false);
    }).catch((e) => {
      setUserDetailError(e);
      setIsLoadingUserDetail(false);
    });
  }, [selectedUser]);
```

```typescript
// src/components/UserDetail.tsx
// revert back to userDetail parameter

interface UserDetailProps {
  userDetail: UserDetail;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  userDetail,
}: UserDetailProps) => {
  return (
    <div className="user-detail">
      <h2>{userDetail.name}</h2>
      <p>Hobby: {userDetail.hobby}</p>
      <p>Age: {userDetail.age}</p>
      <p>Country: {userDetail.country}</p>
      <p>Languages: {userDetail.languages.join(', ')}</p>
      <p>Friends: {userDetail.friends.join(', ')}</p>
    </div>
  );
};
```

- remove the useUserDetail hook

- create a useCachedUserDetail hook

```typescript
// src/hooks/useCachedUserDetail.ts
export const useCachedUserDetail = (selectedUserId: number | null) => {
  const [userDetailCache, setUserDetailCache] = useState<{
    [key: number]: UserDetail;
  }>({});

  const [isLoadingUserDetail, setIsLoadingUserDetail] =
    useState<boolean>(false);
  const [userDetailError, setUserDetailError] = useState<Error | null>(null);
  const [selectedUserDetail, setSelectedUserDetail] =
    useState<UserDetail | null>(null);

  const load = (id: number) => {
    setIsLoadingUserDetail(true);
    setUserDetailError(null);
    fetchUserDetail(id)
      .then((data) => {
        setUserDetailCache((prevCache) => ({
          ...prevCache,
          [id]: data,
        }));
        setSelectedUserDetail(data);
        setIsLoadingUserDetail(false);
      })
      .catch((e) => {
        setUserDetailError(e);
        setIsLoadingUserDetail(false);
      });
  }

  useEffect(() => {
    if (!selectedUserId) {
      return;
    }

    if (userDetailCache[selectedUserId]) {
      setSelectedUserDetail(userDetailCache[selectedUserId]);
      return;
    }

    load(selectedUserId);
  }, [selectedUserId]);


  return { selectedUserDetail, isLoadingUserDetail, userDetailError };
};
```

- use the hook in App component

```typescript
// src/App.tsx
const { selectedUserDetail, isLoadingUserDetail, userDetailError } =
    useCachedUserDetail(selectedUser?.id);
```

### Refreshing Data

- Add a onRefresh helper method to the useCachedUserDetail hook

```typescript
// src/hooks/useCachedUserDetail.ts
export const useCachedUserDetail = (selectedUserId: number | null) => {
  // ...

  const onRefresh = () => {
    if (!selectedUserId) {
      return;
    }
    load(selectedUserId);
  }

  return { selectedUserDetail, isLoadingUserDetail, userDetailError, onRefresh };
};
```

```typescript
// src/App.tsx
const { selectedUserDetail, isLoadingUserDetail, userDetailError, onRefresh } =
    useCachedUserDetail(selectedUser?.id);

return (
  //...
  <UserDetailSection userDetail={selectedUserDetail} onRefresh={onRefresh} />
  //...
)
```

- Add a refresh button to the UserDetail component

```typescript
// src/components/UserDetail.tsx
interface UserDetailProps {
  userDetail: UserDetail;
  onRefresh: () => void;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  userDetail,
  onRefresh,
}: UserDetailProps) => {
  return (
    <div className="user-detail">
      // ...
      <button onClick={onRefresh}>Refresh</button>
    </div>
  );
};
```
````
