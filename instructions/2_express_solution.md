# Simple express server

We will now create a simple express server that we can access using RTK query.

First, make a new directory and initialize npm

```bash
mkdir express_server
cd express_server
npm init -y
```

Initialize git

```bash
git init -b main
touch .gitignore
```

Add Typescript to the project

```bash
npm i -D typescript @types/typescript
npx tsc --init
```

Add express to the project

```bash
npm install express
npm i -D @types/express
```

Install TsNode to run the server

```bash
npm i -D ts-node
```

Install NodeMon to automatically restart the server when we make changes

```bash
npm i -D nodemon
```

Add a 'dev' script to the package.json file

```json
"scripts": {
  "dev": "nodemon index.ts"
}
```

Add base 'src' directory and add 'index.ts' file

```bash
mkdir src
touch src/index.ts
```

Update package.json to point to the 'src/index.ts' file

```json
"main": "src/index.ts",
"scripts": {
  "dev": "nodemon src/index.ts"
}
```

Create the basic server in 'src/index.ts'

```typescript
import express from "express";

const app = express();
const port = 3000;
app.get("/", (req, res) => {
  console.log("request received: ", req.url);
  res.send("Hello World!");
});

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
```

Try running the server

```bash
npm run dev
```

Create some dummy data:

```typescript
// In src/index.ts is fine
const data = [
  { id: 1, name: "John", hobby: "Soccer" },
  { id: 2, name: "Jane", hobby: "Reading" },
  { id: 3, name: "Jim", hobby: "Swimming" },
];
```

Add a new route to the server that returns the dummy data

```typescript
app.get("/users", (req, res) => {
  console.log("request received: ", req.url);
  res.json(data);
});
```

Access users [users](http://localhost:3000/users)

Add a user detail route to the server

```typescript
const userDetails = {
  1: {
    id: 1,
    name: "John",
    hobby: "Soccer",
    age: 25,
    country: "USA",
    languages: ["English", "Spanish"],
    friends: [2],
  },
  2: {
    id: 2,
    name: "Jane",
    hobby: "Reading",
    age: 30,
    country: "UK",
    languages: ["English", "French"],
    friends: [1, 3],
  },
  3: {
    id: 3,
    name: "Jim",
    hobby: "Swimming",
    age: 22,
    country: "Australia",
    languages: ["English"],
    friends: [],
  },
};
```

```typescript
app.get("/users/:id", (req, res) => {
  const id: number = parseInt(req.query.id as string);
  console.log("request user detail: ", id);

  if (userDetails[id]) {
    res.json(userDetails[id]);
  } else {
    res.status(404).send("User not found");
  }
});
```

Try the route

- [user1](http://localhost:3000/users/detail?id=1)
- [user1](http://localhost:3000/users/detail?id=2)
- [user1](http://localhost:3000/users/detail?id=3)
