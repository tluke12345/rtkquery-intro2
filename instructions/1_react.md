# React project

before we can do anythion we need a project to work on. We will create a new
project without using create-react-app. This will give us a better
understanding of the base technologies.

## Create a new project

### Create Project

- Create a new directory for the project and navigate into it.
- Initialize git
- Initialize npm in the project directory.

### TypeScript and React

- Install TypeScript and React.
- Initialize TypeScript in the project.

### Formatting and Linting

- Add EsLint and Prettier to the project.
- Initialize EsLint in the project.
- Make sure to select the 'react' and 'typescript' option when configuring EsLint.
- Setup Prettier in the project.

### Add Vite

- Add vite for easy development.
- Add start, lint, and format scripts to the `package.json` file.
- Change the `main` field in the `package.json` file to point to the
  `index.html` file.

### directory structure

Now we have a basic project setup. We can start adding code to the project.

- First create a `src` directory and add a `main.tsx` file.
- Now add the application entry point, the index.html file.
- Lets add a simple App component
- Now we can initialize our website in the `main.tsx` file.

### Test it out

- Start the APP
- make sure to commit the changes. Make sure you dont commit the node_modules
  directory.
