# Connect with API using fetch

Before we use the tools provided by RTK Query, it is good to understand the
problem we are solving. We well connect the react app with the backend server
using the browser native 'fetch' api.

## Steps

- make a copy of the react app (we will use 3_react_ui as the base for 5 too)

### Repository

- Create a respository

### Fetch Users

- Test that the the we can connect
- useEffect to handle async fetch
- show loading ui
- catch errors and show error message
- custom hooks to organize code (separate view code and logic code
  -> 'controller' or 'view model')

### Fetch User Detail

- Create a repository for fetching user detail
- Create a hook for fetching user detail
- Modify userDetail component with using hook, and show loading and error
- Test the app. Watch the network tab. When you click on a user the detail API
  is called. Note that the same api is called multiple times for the same user.

### Caching

- We need to elevate the state to the App component to cache the user detail
- remove the useUserDetail hook
- create a useCachedUserDetail hook
- use the hook in App component

### Refreshing Data

- Add a onRefresh helper method to the useCachedUserDetail hook
- Add a refresh button to the UserDetail component
