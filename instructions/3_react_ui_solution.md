# Simple react UI

(working in the `react_project` directory)

## Steps

### User

Create a model to represent a user

```bash
mkdir src/models
touch src/models/user.ts
```

```typescript
export interface User {
  id: number;
  name: string;
}
```

Create a User 'Card' component that displays the user information

```bash
mkdir src/components
touch src/components/UserCard.tsx
```

```tsx
import React from "react";
import { User } from "../models/user";

interface UserCardProps {
  user: User;
}

export const UserCard: React.FC<UserCardProps> = ({ user }: UserCardProps) => {
  return (
    <div className="card">
      <h2>{user.name}</h2>
    </div>
  );
};
```

Create a css file and style resets, variables, etc.

```bash
touch public/css/styles.css
```

```css
/* VARIABLES */
:root {
  --font-size: 16px;
  --font-family: Arial, sans-serif;
  --gap: 20px;
}

/* RESET */
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  background-color: #f4f4f4;
  font-size: var(--font-size);
  font-family: var(--font-family);
  padding: var(--gap);
  min-height: 100vh;
}

#app {
  width: min-content;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  gap: var(--gap);
}
```

```html
<link rel="stylesheet" href="css/styles.css" />
```

Add Some card styles

```css
.card {
  width: 300px;
  margin: 20px;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 5px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}
```

Add some hard coded data to the App component and display the UserCards

```tsx
// Temporary data
const users: User[] = [
  { id: 1, name: "Alice", hobby: "Reading" },
  { id: 2, name: "Bob", hobby: "Swimming" },
  { id: 3, name: "Charlie", hobby: "Running" },
];

// Return the UserCards
return (
  <div className="user-list">
    <h1>Users</h1>
    {users.map((user) => (
      <UserCard key={user.id} user={user} />
    ))}
  </div>
);
```

### User Detail

Create a userDetail model

```bash
touch src/models/userDetail.ts
```

```typescript
export interface UserDetail {
  id: number;
  name: string;
  hobby: string;
  age: number;
  country: string;
  languages: string[];
  friends: number[];
}
```

Create a user detail component

```bash
touch src/components/UserDetail.tsx
```

```tsx
import React from "react";
import { UserDetail } from "../models/userDetail";

interface UserDetailProps {
  userDetail: UserDetail;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  userDetail,
}: UserDetailProps) => {
  return (
    <div className="user-detail">
      <h2>{userDetail.name}</h2>
      <p>Hobby: {userDetail.hobby}</p>
      <p>Age: {userDetail.age}</p>
      <p>Country: {userDetail.country}</p>
      <p>Languages: {userDetail.languages.join(", ")}</p>
      <p>Friends: {userDetail.friends.join(", ")}</p>
    </div>
  );
};
```

Add some styles for the user detail section

```css
.user-detail {
  display: flex;
  flex-direction: column;
  padding: 10px;
}

.user-detail > p {
  padding: 10px 10px 0 10px;
}
```

Add some hard coded data to the App component and display the UserDetail

```tsx
const userDetails: { [key: number]: userDetail } = {
  1: {
    id: 1,
    name: "John",
    hobby: "Soccer",
    age: 25,
    country: "USA",
    languages: ["English", "Spanish"],
    friends: [2],
  },
  2: {
    id: 2,
    name: "Jane",
    hobby: "Reading",
    age: 30,
    country: "UK",
    languages: ["English", "French"],
    friends: [1, 3],
  },
  3: {
    id: 3,
    name: "Jim",
    hobby: "Swimming",
    age: 22,
    country: "Australia",
    languages: ["English"],
    friends: [],
  },
};
```

Add a user detail section after the list of user cards. This should display
the currently selected user.

```tsx
const [selectedUser, setSelectedUser] = useState<User | null>(null);

const selectedUserDetail =
  selectedUser !== null ? userDetails[selectedUser.id] : null;

return (
  // ...
  <hr />
  {selectedUserDetail ? (
    <UserDetailSection userDetail={selectedUserDetail} />
  ) : (
    <p>Select a user</p>
  )}
);
```

Set the selected user by clicking on the user card

```tsx
// (UserCard.tsx)
interface UserCardProps {
  user: User;
  onClick?: () => void;
}

export const UserCard: React.FC<UserCardProps> = ({
  user,
  onClick,
}: UserCardProps) => {
  return (
    <div className="card" onClick={onClick}>
      <h2>{user.name}</h2>
    </div>
  );
};
```

```tsx
// (App.tsx)
const handleClick = (user: User) => {
  setSelectedUser(user);
};

return {
  // ...
  <UserCard key={user.id} user={user} onClick={() => handleClick(user)} />
};
```

Modify the code so that the user card can be clicked again to deselect the user

```tsx
// (App.tsx)
const handleClick = (user: User) => {
  setSelectedUser((prevUser) => (prevUser?.id === user.id ? null : user));
};
```

Add border around the selected user card

```tsx
// (App.tsx)
return {
  // ...
  <UserCard key={user.id} user={user} onClick={() => handleClick(user)} selected={selectedUser?.id === user.id} />
};
```

```css
.card.selected {
  border-color: blue;
}
```

```tsx
// (UserCard.tsx)
interface UserCardProps {
  user: User;
  onClick?: () => void;
  selected?: boolean;
}

export const UserCard: React.FC<UserCardProps> = ({
  user,
  onClick,
  selected,
}: UserCardProps) => {
  return (
    <div className={`card ${selected ? "selected" : ""}`} onClick={onClick}>
      <h2>{user.name}</h2>
    </div>
  );
};
```
