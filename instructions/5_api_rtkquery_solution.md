# Connect with the API using RTK query

Now that we understand the problem, lets investigate how rtk and rtk query
can help.

## step

### Copy 3_react_ui to 5_api_rtkquery

```bash
cp -r 3_react_ui 5_api_rtkquery
cd 5_api_rtkquery
```

### Add rtk

- Add rtk to the project

```bash
npm install @reduxjs/toolkit react-redux
```

- Implement the api

```typescript
// src/store/userApi.ts
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const userURL = "http://localhost:3001/users";

export const userApi = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({ baseUrl: userURL }),
  endpoints: (builder) => ({
    getUsers: builder.query({
      query: () => "",
    }),
    getUserDetails: builder.query({
      query: (id: string) => `detail?id=${id}`,
    }),
  }),
});

export const { useGetUsersQuery, useGetUserDetailsQuery } = api;
```

- Create redux store and add the api to it

```typescript
// src/store/store.ts
import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { userApi } from "./userApi";

export const store = configureStore({
  reducer: {
    [userApi.reducerPath]: userApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(userApi.middleware),
});

setupListeners(store.dispatch);
```

- Add the store to the app

```typescript
// src/main.tsx
import React from "react";
import { createRoot } from "react-dom/client"
import { App } from "./App";
import { Provider } from "react-redux";
import { store } from "./store/store";

const appElement = document.getElementById("app") as HTMLElement;
const root = createRoot(appElement)
root.render(
  <Provider store={store}>
    <App />
  </Provider>
)
```

- test the new api

```typescript
// src/App.tsx
const { data, error, isLoading } = useGetUsersQuery("");
console.log("isLoading", isLoading);
console.log("error", error);
console.log("data", data);
```

### User API

- use rtk query to fetch data, loading and error state

````typescript
// src/App.tsx

export const App = () => {
  const { data: users, error: userError, isLoading: userIsLoading } = useGetUsersQuery();
  /..

  return (
    // ...
    <div className="user-list">
      {userIsLoading && <p>Loading...</p>}
      {userError && <p>Error fetching data</p>}
      <h1>Users</h1>
      {users && users.map((user) =>
        <UserCard
          key={user.id}
          user={user}
          onClick={() => handleClick(user)}
          selected={selectedUser?.id === user.id} />
      )}
    </div>
    // ..
  )
}

### User Details

- add a new endpoint to api

```typescript
// src/store/userApi.ts
export const userApi = createApi({
  //..
  endpoints: (builder) => ({
    //...
    getUserDetails: builder.query<UserDetail, number>({
      query: (id: number) => `/detail?id=${id}`,
    }),
  }),
})

export const { useGetUsersQuery, useGetUserDetailsQuery } = userApi;
````

- Test the new endpoint

```typescript
// src/App.tsx
const {
  data: userDetails,
  error: userDetailsError,
  isLoading: userDetailsIsLoading,
} = useGetUserDetailsQuery(1);
console.log("userDetailsIsLoading", userDetailsIsLoading);
console.log("userDetailsError", userDetailsError);
console.log("userDetails", userDetails);
```

- Call the new endpoint when a user is selected

```typescript
// src/App.tsx
// pass the id to the UserDetailSection
export const App = () => {
  //...
  return (
    // ...
    {selectedUser ? (
      <UserDetailSection id={selectedUser.id} />
    ) : (
      <p>Select a user</p>
    )}
  )
}
```

```typescript
// src/UserDetailSection.tsx
interface UserDetailProps {
  id: number;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  id,
}: UserDetailProps) => {
  const { data: userDetail, error, isLoading } = useGetUserDetailsQuery(id);
  return (
    <>
      {isLoading && <p>Loading...</p>}
      {error && <p>Error fetching data</p>}
      {userDetail && (
        <div className="user-detail">
          <h2>{userDetail.name}</h2>
          <p>Hobby: {userDetail.hobby}</p>
          <p>Age: {userDetail.age}</p>
          <p>Country: {userDetail.country}</p>
          <p>Languages: {userDetail.languages.join(', ')}</p>
          <p>Friends: {userDetail.friends.join(', ')}</p>
        </div>
      )}
    </>
  );
};
```

### Reloading data

- Add a button to reload the data

```typescript
// src/components/UserDetailSection.tsx
const { data: userDetail, error, isLoading, refetch } = useGetUserDetailsQuery(id);

return (
  // ...
  <button onClick={refetch}>Refresh</button>
  // ...
)
```
