import React, { useState } from 'react';
import { UserCard } from './components/UserCard';
import { User } from './models/user';
import { UserDetailSection } from './components/UserDetail';
import { UserDetail } from './models/userDetail';

export const App = () => {
  const [selectedUser, setSelectedUser] = useState<User | null>(null);

  // Temporary data
  const users: User[] = [
    { id: 1, name: 'Alice', hobby: 'Reading' },
    { id: 2, name: 'Bob', hobby: 'Swimming' },
    { id: 3, name: 'Charlie', hobby: 'Running' }
  ];
  const userDetails: Record<number, UserDetail> = {
    1: {
      id: 1,
      name: 'John',
      hobby: 'Soccer',
      age: 25,
      country: 'USA',
      languages: ['English', 'Spanish'],
      friends: [2]
    },
    2: {
      id: 2,
      name: 'Jane',
      hobby: 'Reading',
      age: 30,
      country: 'UK',
      languages: ['English', 'French'],
      friends: [1, 3]
    },
    3: {
      id: 3,
      name: 'Jim',
      hobby: 'Swimming',
      age: 22,
      country: 'Australia',
      languages: ['English'],
      friends: []
    }
  };

  const handleClick = (user: User) => {
    setSelectedUser((prevUser) => (prevUser?.id === user.id ? null : user));
  };

  const selectedUserDetail =
    selectedUser !== null ? userDetails[selectedUser.id] : null;

  return (
    <>
      <div className="user-list">
        <h1>Users</h1>
        {users.map((user) => (
          <UserCard
            key={user.id}
            user={user}
            onClick={() => handleClick(user)}
            selected={selectedUser?.id === user.id}
          />
        ))}
      </div>
      <hr />
      {selectedUserDetail ? (
        <UserDetailSection userDetail={selectedUserDetail} />
      ) : (
        <p>Select a user</p>
      )}
    </>
  );
};
