import React from 'react';
import { UserDetail } from '../models/userDetail';

interface UserDetailProps {
  userDetail: UserDetail;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  userDetail
}: UserDetailProps) => {
  return (
    <div className="user-detail">
      <h2>{userDetail.name}</h2>
      <p>Hobby: {userDetail.hobby}</p>
      <p>Age: {userDetail.age}</p>
      <p>Country: {userDetail.country}</p>
      <p>Languages: {userDetail.languages.join(', ')}</p>
      <p>Friends: {userDetail.friends.join(', ')}</p>
    </div>
  );
};
