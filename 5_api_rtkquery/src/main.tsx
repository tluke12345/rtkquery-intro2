import React from 'react';
import { createRoot } from 'react-dom/client';
import { App } from './App';
import { Provider } from 'react-redux';
import { store } from './store/store';

const appElement = document.getElementById('app') as HTMLElement;
const root = createRoot(appElement);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
