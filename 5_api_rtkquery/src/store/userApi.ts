import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from '../models/user';
import { UserDetail } from '../models/userDetail';

const userURL = 'http://localhost:3000/users';

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: fetchBaseQuery({ baseUrl: userURL }),
  endpoints: (builder) => ({
    getUsers: builder.query<User[], void>({
      query: () => ''
    }),
    getUserDetails: builder.query<UserDetail, number>({
      query: (id: number) => `/detail?id=${id}`
    })
  })
});
export const { useGetUsersQuery, useGetUserDetailsQuery } = userApi;
