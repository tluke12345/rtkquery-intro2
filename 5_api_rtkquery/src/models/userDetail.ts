export interface UserDetail {
  id: number;
  name: string;
  hobby: string;
  age: number;
  country: string;
  languages: string[];
  friends: number[];
}
