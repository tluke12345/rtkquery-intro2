import React, { useState } from 'react';
import { UserCard } from './components/UserCard';
import { User } from './models/user';
import { UserDetailSection } from './components/UserDetail';
import { useGetUsersQuery } from './store/userApi';

export const App = () => {
  const [selectedUser, setSelectedUser] = useState<User | null>(null);
  const {
    data: users,
    error: userError,
    isLoading: userIsLoading
  } = useGetUsersQuery();

  const handleClick = (user: User) => {
    setSelectedUser((prevUser) => (prevUser?.id === user.id ? null : user));
  };

  return (
    <>
      <div className="user-list">
        {userIsLoading && <p>Loading...</p>}
        {userError && <p>Error fetching data</p>}
        <h1>Users</h1>
        {users &&
          users.map((user) => (
            <UserCard
              key={user.id}
              user={user}
              onClick={() => handleClick(user)}
              selected={selectedUser?.id === user.id}
            />
          ))}
      </div>
      <hr />
      {selectedUser ? (
        <UserDetailSection id={selectedUser.id} />
      ) : (
        <p>Select a user</p>
      )}
    </>
  );
};
