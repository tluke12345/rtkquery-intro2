import React from 'react';
import {
  useGetUserDetailsQuery,
  useGetUserDetailsQueryi
} from '../store/userApi';

interface UserDetailProps {
  id: number;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  id
}: UserDetailProps) => {
  const {
    data: userDetail,
    error,
    isLoading,
    refetch
  } = useGetUserDetailsQuery(id);
  return (
    <>
      {isLoading && <p>Loading...</p>}
      {error && <p>Error fetching data</p>}
      {userDetail && (
        <div className="user-detail">
          <h2>{userDetail.name}</h2>
          <p>Hobby: {userDetail.hobby}</p>
          <p>Age: {userDetail.age}</p>
          <p>Country: {userDetail.country}</p>
          <p>Languages: {userDetail.languages.join(', ')}</p>
          <p>Friends: {userDetail.friends.join(', ')}</p>
          <button onClick={refetch}>Refresh</button>
        </div>
      )}
    </>
  );
};
