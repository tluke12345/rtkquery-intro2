import { useEffect, useState } from 'react';
import { UserDetail } from '../models/userDetail';
import { fetchUserDetail } from '../repositories/userRepository';

export const useCachedUserDetail = (selectedUserId: number | null) => {
  const [userDetailCache, setUserDetailCache] = useState<{
    [key: number]: UserDetail;
  }>({});

  const [isLoadingUserDetail, setIsLoadingUserDetail] =
    useState<boolean>(false);
  const [userDetailError, setUserDetailError] = useState<Error | null>(null);
  const [selectedUserDetail, setSelectedUserDetail] =
    useState<UserDetail | null>(null);

  const load = (id: number) => {
    setIsLoadingUserDetail(true);
    setUserDetailError(null);
    setSelectedUserDetail(null);
    fetchUserDetail(id)
      .then((data) => {
        setUserDetailCache((prevCache) => ({
          ...prevCache,
          [id]: data
        }));
        setSelectedUserDetail(data);
        setIsLoadingUserDetail(false);
      })
      .catch((e) => {
        setUserDetailError(e);
        setIsLoadingUserDetail(false);
      });
  };

  useEffect(() => {
    if (!selectedUserId) {
      return;
    }

    if (userDetailCache[selectedUserId]) {
      setSelectedUserDetail(userDetailCache[selectedUserId]);
      return;
    }

    load(selectedUserId);
  }, [selectedUserId]);

  const onRefresh = () => {
    if (!selectedUserId) {
      return;
    }
    load(selectedUserId);
  };

  return {
    selectedUserDetail,
    isLoadingUserDetail,
    userDetailError,
    onRefresh
  };
};
