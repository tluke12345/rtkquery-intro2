import React, { useState } from 'react';
import { UserCard } from './components/UserCard';
import { useUsers } from './hooks/useUsers';
import { User } from './models/user';
import { UserDetailSection } from './components/UserDetail';
import { useCachedUserDetail } from './hooks/useCachedUserDetail';

export const App = () => {
  const [selectedUser, setSelectedUser] = useState<User | undefined>(undefined);
  const { users, loading, error } = useUsers();
  const {
    selectedUserDetail,
    isLoadingUserDetail,
    userDetailError,
    onRefresh
  } = useCachedUserDetail(selectedUser?.id);

  const handleClick = (user: User) => {
    setSelectedUser((prevUser) =>
      prevUser?.id === user.id ? undefined : user
    );
  };

  return (
    <>
      <div className="user-list">
        <h1>Users</h1>
        {loading && <p>Loading...</p>}
        {error && <p>Error: {error.message}</p>}
        {users.map((user) => (
          <UserCard
            key={user.id}
            user={user}
            onClick={() => handleClick(user)}
            selected={selectedUser?.id === user.id}
          />
        ))}
      </div>
      <hr />
      {isLoadingUserDetail && <p>Loading user detail...</p>}
      {userDetailError && <p>Error: {userDetailError.message}</p>}
      {selectedUser && selectedUserDetail && (
        <UserDetailSection
          userDetail={selectedUserDetail}
          onRefresh={onRefresh}
        />
      )}
    </>
  );
};
