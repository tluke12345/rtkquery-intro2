import React from 'react';
import { UserDetail } from '../models/userDetail';

interface UserDetailProps {
  userDetail: UserDetail;
  onRefresh: () => void;
}

export const UserDetailSection: React.FC<UserDetailProps> = ({
  userDetail,
  onRefresh
}: UserDetailProps) => {
  return (
    <div className="user-detail">
      <h2>{userDetail.name}</h2>
      <p>Hobby: {userDetail.hobby}</p>
      <p>Age: {userDetail.age}</p>
      <p>Country: {userDetail.country}</p>
      <p>Languages: {userDetail.languages.join(', ')}</p>
      <p>Friends: {userDetail.friends.join(', ')}</p>
      <button onClick={onRefresh}>Refresh</button>
    </div>
  );
};
