import { User } from '../models/user';
import { UserDetail } from '../models/userDetail';

const userURL = 'http://localhost:3000/users';
const userDetailURL = 'http://localhost:3000/users/detail';

export const fetchUsers = async (): Promise<User[]> => {
  const response = await fetch(userURL);
  if (!response.ok) {
    throw new Error('Failed to fetch users');
  }
  return (await response.json()) as User[];
};

export const fetchUserDetail = async (id: number): Promise<UserDetail> => {
  const response = await fetch(`${userDetailURL}?id=${id}`);
  if (!response.ok) {
    throw new Error('Failed to fetch user');
  }
  return (await response.json()) as UserDetail;
};
