import React from 'react';
import { createRoot } from 'react-dom/client';
import { App } from './App';

const appElement = document.getElementById('app') as HTMLElement;
const root = createRoot(appElement);
root.render(<App />);
