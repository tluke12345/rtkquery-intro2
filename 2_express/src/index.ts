import express from "express";

const app = express();
const port = 3000;

type User = {
  id: number;
  name: string;
};
type userDetail = {
  id: number;
  name: string;
  hobby: string;
  age: number;
  country: string;
  languages: string[];
  friends: number[];
};

const data: User[] = [
  { id: 1, name: "John" },
  { id: 2, name: "Jane" },
  { id: 3, name: "Jim" },
];

const userDetails: { [key: number]: userDetail } = {
  1: {
    id: 1,
    name: "John",
    hobby: "Soccer",
    age: 25,
    country: "USA",
    languages: ["English", "Spanish"],
    friends: [2],
  },
  2: {
    id: 2,
    name: "Jane",
    hobby: "Reading",
    age: 30,
    country: "UK",
    languages: ["English", "French"],
    friends: [1, 3],
  },
  3: {
    id: 3,
    name: "Jim",
    hobby: "Swimming",
    age: 22,
    country: "Australia",
    languages: ["English"],
    friends: [],
  },
};

app.get("/", (req, res) => {
  console.log("received request: ", req.url);
  res.send("hello server");
});

app.get("/users", (req, res) => {
  console.log("request received: ", req.url);
  res.json(data);
});

app.get("/users/detail", (req, res) => {
  const id: number = parseInt(req.query.id as string);
  console.log("request user detail: ", id);

  if (userDetails[id]) {
    res.json(userDetails[id]);
  } else {
    res.status(404).send("User not found");
  }
});

app.listen(port, () => {
  console.log(`Server listenting at http://localhost:${port}`);
});
